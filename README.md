# CICD Untuk Ujian Saringan Masuk Suitmedia
![Pipelines](images/pipelines.png)

Maaf saya saat ini tidak memiliki akun AWS yang aktif untuk membuat deploy stage ke Cloud. Saya menggunakan Laptop Macbook saya sebagai Gitlab Runner dan manager node dari Docker Swarm untuk deployment 

## Gitlab CI CD Variables yang diperlukan
- `CI_REGISTRY` : Registry Docker. Contoh: `docker.io`
- `CI_REGISTRY_IMAGE`: Registry DockerHub repo. Contoh: `docker.io/arisnacg/web-cicd-example`
- `CI_REGISTRY_USER`: DockerHub username. Contoh: `arisnacg`
- `CI_REGISTRY_PASSWORD`: DockerHub password atau access token. Contoh: `dckr_pat_swCUB9_XXXX`
- `SONAR_HOST_URL`: Sonarqube host url. Contoh: `sonarqube.example.com`
- `SONAR_TOKEN`: Sonarqube token. Contoh: `sqp_f611XXXX`
