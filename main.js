import './style.css';
import { setupTodos } from './todos';

document.querySelector('#app').innerHTML = `
  <div>
    <h1>Web CICD Example!</h1>
    <div id="list-todo">
      <p>1. Makan</p>
      <p>2. Minum</p>
    </div>
    <div>
      <input type="text" id="input-text" />
      <button id="button-add">Add</button>
    </div>
  </div>
`;
setupTodos(document.querySelector("#button-add"));
