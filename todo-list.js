export class TodoList {
  todos = [];
  getTodos = function () {
    return this.todos;
  };

  getLastTodo() {
    return this.todos[this.todos.length - 1];
  }

  addTodo = function (todoName) {
    this.todos.push(todoName);
    return todoName;
  };

  getTodoLength = function () {
    return this.todos.length;
  };
}