# Use multistage to reduce Docker image size

# Build stage
FROM node:18 as build
WORKDIR /app
COPY package.json package.json
COPY yarn.lock yarn.lock
RUN yarn install --frozen-lockfile
COPY . .
RUN yarn build

# Host stage
FROM nginx:latest as prod
COPY --from=build /app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]