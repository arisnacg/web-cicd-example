/// <reference types="cypress" />

describe('example to-do app', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8080');
  });

  it('displays zero todo by default', () => {
    cy.get('#list-todo > p').should('have.length', 0);
  });

  it('can add new todo items', () => {
    const arrNewTodos = ['Sleep 8 hours', 'Eat 3 meals', 'Drink more water'];
    for (let index = 0; index < arrNewTodos.length; index++) {
      const todo = arrNewTodos[index];
      cy.get('#input-text').clear();
      cy.get('#input-text').type(`${todo}`);
      cy.get('#button-add').click();

      cy.get('#list-todo p')
        .should('have.length', index + 1)
        .last()
        .should('have.text', `${index + 1}. ${todo}`);
    }
  });

});
