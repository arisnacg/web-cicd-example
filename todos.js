import { TodoList } from "./todo-list";

const todoList = new TodoList();

export function addNewTodo(newTodoName) {
  todoList.addTodo(newTodoName);
  const todoLength = todoList.getTodoLength();
  return `${todoLength}. ${newTodoName}`;
}

export function getAllTodo() {
  return todoList.getTodos();
}

export function setupTodos(element) {
  const showTodo = () => {
    const todos = getAllTodo();
    let todoHTML = ``;
    todos.forEach((e, i) => {
      todoHTML += `<p>${i + 1}. ${e}`;
    });
    document.querySelector('#list-todo').innerHTML = todoHTML;
  };

  const addTodo = () => {
    const newTodoName = document.querySelector("#input-text").value;
    if (newTodoName !== "") {
      addNewTodo(newTodoName);
      showTodo();
    }
  };
  element.addEventListener('click', () => addTodo());
  showTodo();
}
