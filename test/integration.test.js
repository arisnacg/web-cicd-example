import { expect, test } from 'vitest';
import { addNewTodo, getAllTodo } from '../todos';

test('Add 3 new todos', () => {
  const newTodos = ['Eat', 'Sleep', 'Game'];
  newTodos.forEach(newTodo => {
    addNewTodo(newTodo);
  });
  const todos = getAllTodo();
  for (let i = 0; i < todos.length; i++) {
    expect(todos[i]).toBe(newTodos[i]);
  }
});