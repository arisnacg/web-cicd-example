import { expect, test } from 'vitest';
import { TodoList } from '../todo-list';

test('add new todo to TodoList class', () => {
  const todoList = new TodoList();
  const newTodo = `Sleep 8 hours`;
  todoList.addTodo(newTodo);
  expect(todoList.getLastTodo()).toBe(newTodo);
});